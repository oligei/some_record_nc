select * from md_class where displayname like '%付款单%'

select * from md_class where defaulttablename like '%jzpr_payplan_acc%'

select vname,PK_FEEITEM from jzbd_feeitem

select * from  jzpr_payplan_acc where dr!=1
select * from  jzpr_payplan where pk_payplan='1001A21000000000DASR'
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--付款申请单表体  付款计划表体主键为空
select * from jzcm_PayAppFrmB --
select * from jzcm_PayAppFrmB B where B.pk_mnytype = '1001A210000000006RX0' and dr!=1 and nvl(Pk_bdefdoc1,'~')='~' --结算         jzpr_payplan_settle  8
select * from jzcm_PayAppFrmB B where B.pk_mnytype = '1001A210000000006RX1' and dr!=1 and nvl(Pk_bdefdoc1,'~')='~' --进度--其它款 jzpr_payplan_oth     65
select * from jzcm_PayAppFrmB B where  dr!=1 and nvl(Pk_bdefdoc1,'~')='~' --73

--付款申请单表体  合同pk与付款计划表体pk,不存在同时为null情况 
select * from jzcm_PayAppFrmB where dr!=1 and  pk_mnytype in ('1001A210000000006RX0','1001A210000000006RX1') and nvl(Pk_bdefdoc5,'~')='~' and nvl(Pk_bdefdoc1,'~')='~'
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--付款计划结算款表体，写道付款申请单表体    符合常规-5条
select B.PK_PAYAPPFRM_B,B.Pk_bdefdoc1,J.PK_PAYPLAN_SETTLE,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX0' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_SETTLE,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_settle j where dr!=1 and pk_feeitem='1001A210000000006RX0') J 
where B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT

merge into jzcm_PayAppFrmB JB
using (
select B.PK_PAYAPPFRM_B pk,B.Pk_bdefdoc1,J.PK_PAYPLAN_SETTLE,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX0' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_SETTLE,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_settle j where dr!=1 and pk_feeitem='1001A210000000006RX0') J 
where B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT
) temp 
on (JB.PK_PAYAPPFRM_B=temp.pk)
when matched then 
  update  set JB.PK_BDEFDOC1 = temp.PK_PAYPLAN_SETTLE;

--付款计划结算款表体，写道付款申请单表体    非常规-3条，结算款填写在进度款上
select B.PK_PAYAPPFRM_B,B.Pk_bdefdoc1,J.PK_PAYPLAN_OTH,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX0' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_OTH,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_oth j where dr!=1 and pk_feeitem='1001A210000000006RX0') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT

merge into jzcm_PayAppFrmB JB
using (
select B.PK_PAYAPPFRM_B pk,B.Pk_bdefdoc1,J.PK_PAYPLAN_OTH,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX0' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_OTH,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_oth j where dr!=1 and pk_feeitem='1001A210000000006RX0') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT
) temp 
on (JB.PK_PAYAPPFRM_B=temp.pk)
when matched then 
  update  set JB.PK_BDEFDOC1 = temp.PK_PAYPLAN_OTH;

--付款计划进度款表体，写道付款申请单表体    符合常规-64条
select B.PK_PAYAPPFRM_B,B.Pk_bdefdoc1,J.PK_PAYPLAN_OTH,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX1' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_OTH,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_oth j where dr!=1 and pk_feeitem='1001A210000000006RX1') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT

merge into jzcm_PayAppFrmB JB
using (
select B.PK_PAYAPPFRM_B pk,B.Pk_bdefdoc1,J.PK_PAYPLAN_OTH,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX1' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_OTH,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_oth j where dr!=1 and pk_feeitem='1001A210000000006RX1') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT
) temp 
on (JB.PK_PAYAPPFRM_B=temp.pk)
when matched then 
  update  set JB.PK_BDEFDOC1 = temp.PK_PAYPLAN_OTH;

--付款计划进度款表体，写道付款申请单表体    非常规-1条 进度款填写在结算款上
select B.PK_PAYAPPFRM_B,B.Pk_bdefdoc1,J.PK_PAYPLAN_SETTLE,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX1' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_SETTLE,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_settle j where dr!=1 and pk_feeitem='1001A210000000006RX1') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT

merge into jzcm_PayAppFrmB JB
using (
select B.PK_PAYAPPFRM_B pk,B.Pk_bdefdoc1,J.PK_PAYPLAN_SETTLE,B.Pk_bdefdoc2,J.PK_PAYPLAN from 
(select b.Pk_bdefdoc2,b.Pk_bdefdoc5,b.PK_PAYAPPFRM_B,Pk_bdefdoc1 from jzcm_PayAppFrmB b where b.pk_mnytype = '1001A210000000006RX1' and b.dr!=1 and nvl(b.Pk_bdefdoc1,'~')='~') B,
(SELECT j.PK_PAYPLAN_SETTLE,j.PK_CONTRACT,j.PK_PAYPLAN from jzpr_payplan_settle j where dr!=1 and pk_feeitem='1001A210000000006RX1') J 
where  B.Pk_bdefdoc2=J.PK_PAYPLAN and B.Pk_bdefdoc5=J.PK_CONTRACT
) temp 
on (JB.PK_PAYAPPFRM_B=temp.pk)
when matched then 
  update  set JB.PK_BDEFDOC1 = temp.PK_PAYPLAN_SETTLE;	

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--付款单合同pk找回    后面发现丢失的合同pk均来自进度款oth表，有一条非常规数据
select * from jzcm_PayAppFrmB where dr!=1 and  pk_mnytype in ('1001A210000000006RX0','1001A210000000006RX1') and nvl(Pk_bdefdoc5,'~')='~' --33条
select * from jzcm_PayAppFrmB where dr!=1 and  pk_mnytype='1001A210000000006RX0' and nvl(Pk_bdefdoc5,'~')='~' --1条
select * from jzcm_PayAppFrmB where dr!=1 and  pk_mnytype='1001A210000000006RX1' and nvl(Pk_bdefdoc5,'~')='~' --32条
select Pk_bdefdoc5,count(1) from jzcm_PayAppFrmB where dr!=1 group by Pk_bdefdoc5

--付款申请单表体合同来自进度款    非常规1，进度款表体的结算款
select b.Pk_bdefdoc1,j.PK_CONTRACT,b.Pk_bdefdoc5 from jzcm_PayAppFrmB b,jzpr_payplan_oth j where  nvl(b.Pk_bdefdoc5,'~')='~' and b.pk_mnytype='1001A210000000006RX0' and b.dr!=1 and b.PK_bDEFDOC1=j.PK_PAYPLAN_OTH

merge into jzcm_PayAppFrmB
using (select b.Pk_bdefdoc1,j.PK_CONTRACT from jzcm_PayAppFrmB b,jzpr_payplan_oth j where  nvl(b.Pk_bdefdoc5,'~')='~' and b.pk_mnytype='1001A210000000006RX0' and b.dr!=1 and b.PK_bDEFDOC1=j.PK_PAYPLAN_OTH) t
on (t.Pk_bdefdoc1 = jzcm_PayAppFrmB.PK_BDEFDOC1 )
when matched then 
  update  set jzcm_PayAppFrmB.Pk_bdefdoc5 = t.PK_CONTRACT;

--付款申请单表体合同来自进度款    常规32
select b.Pk_bdefdoc1,j.PK_CONTRACT from jzcm_PayAppFrmB b,jzpr_payplan_oth j where nvl(b.Pk_bdefdoc5,'~')='~' and b.pk_mnytype='1001A210000000006RX1' and b.dr!=1 and b.PK_bDEFDOC1=j.PK_PAYPLAN_OTH

SELECT * from  jzcm_PayAppFrmB JB where JB.dr!=1 and JB.PK_BDEFDOC1  in
(select b.Pk_bdefdoc1 from jzcm_PayAppFrmB b,jzpr_payplan_oth j where nvl(b.Pk_bdefdoc5,'~')='~' and b.pk_mnytype='1001A210000000006RX1' and b.dr!=1 and b.PK_bDEFDOC1=j.PK_PAYPLAN_OTH)

merge into jzcm_PayAppFrmB
using (select b.Pk_bdefdoc1,j.PK_CONTRACT,j.PK_PAYPLAN_OTH from jzcm_PayAppFrmB b,jzpr_payplan_oth j where nvl(b.Pk_bdefdoc5,'~')='~' and b.pk_mnytype='1001A210000000006RX1' and b.dr!=1 and b.PK_bDEFDOC1=j.PK_PAYPLAN_OTH) t
on (t.PK_PAYPLAN_OTH = jzcm_PayAppFrmB.PK_BDEFDOC1  and jzcm_PayAppFrmB.dr!=1)
when matched then 
  update  set jzcm_PayAppFrmB.Pk_bdefdoc5 = t.PK_CONTRACT;
	
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--付款计划表体pk    付款单付款计划表体pk--vbdef3
select vbdef1,ts,dr from jzpr_pay_details where  nvl(vbdef1,'~')='~' and dr!=1  --vbdef1付款申请单表体pk
select * from jzpr_pay_details where  nvl(vbdef3,'~')='~' and dr!=1

--将付款申请单上的付款计划表体pk--Pk_bdefdoc1 更新到付款单上的付款计划表体pk--vbdef3  --增量操作
select j.PK_PAY_DETAILS,b.pk_PayAppFrm_b,b.Pk_bdefdoc1 from (select * from jzpr_pay_details where  nvl(vbdef3,'~')='~')  j ,jzcm_PayAppFrmB b where j.vbdef1 = b.pk_PayAppFrm_b; 

merge into jzpr_pay_details
using (select j.PK_PAY_DETAILS,b.pk_PayAppFrm_b,b.Pk_bdefdoc1 from (select * from jzpr_pay_details where  nvl(vbdef3,'~')='~')  j ,jzcm_PayAppFrmB b where j.vbdef1 = b.pk_PayAppFrm_b
) t
on (t.PK_PAY_DETAILS = jzpr_pay_details.PK_PAY_DETAILS )
when matched then 
  update  set jzpr_pay_details.vbdef3 = t.Pk_bdefdoc1;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--付款申请单的本次申请已付金额vbdef4        norigpaymny付款单表体付款金额
select B.vbdef4,J.norigpaymny,J.vbdef1, B.pk_PayAppFrm_b from  jzpr_pay_details J left join jzcm_PayAppFrmB B on J.vbdef1 = B.pk_PayAppFrm_b and J.dr!=1 where J.norigpaymny !=	B.vbdef4 and  nvl(J.vbdef1,'~')!='~'; 

select count(1) from jzpr_pay_details where nvl(vbdef1,'~')='~' and dr!=1

--回写付款申请单的本次申请已付金额vbdef4 --全量操作-------危险
merge into jzcm_PayAppFrmB
using (select sum(J.norigpaymny) norigpaymny,J.vbdef1 from jzpr_pay_details J where nvl(J.vbdef1,'~')!='~' and dr!=1 group by J.vbdef1) t
on (t.vbdef1 = jzcm_PayAppFrmB.pk_PayAppFrm_b )
when matched then 
  update  set jzcm_PayAppFrmB.vbdef4 = t.norigpaymny;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
select * from jzpr_pay_details J where J.dr!=1 --付款单条数查看
select J.PK_PAY_DETAILS, B.PK_BDEFDOC5 from  jzpr_pay_details J left join jzcm_PayAppFrmB B on J.vbdef1 = B.pk_PayAppFrm_b where J.dr!=1
select * from jzpr_pay_details where vbdef20='~' and dr!=1

--付款申请单上的合同pk  存储到付款单的vbdef20
merge into jzpr_pay_details
using (select J.PK_PAY_DETAILS, B.PK_BDEFDOC5 from  jzpr_pay_details J left join jzcm_PayAppFrmB B on J.vbdef1 = B.pk_PayAppFrm_b where J.dr!=1) t
on (jzpr_pay_details.PK_PAY_DETAILS = t.PK_PAY_DETAILS )
when matched then 
  update  set jzpr_pay_details.vbdef20 = t.PK_BDEFDOC5;

select pay.vbdef20,pay.norigpaymny from jzpr_pay_details pay,JZSUB_CONTRACT ct where pay.vbdef20 = ct.pk_subcontract 

--付款单表头单据状态写道表体vbdef19
merge into jzpr_pay_details
using (SELECT d.pk_pay,d.PK_PAY_DETAILS,p.FSTATUSFLAG from jzpr_pay_details d,jzpr_pay p where d.pk_pay = p.pk_pay and d.dr!=1) t
on (jzpr_pay_details.PK_PAY_DETAILS = t.PK_PAY_DETAILS )
when matched then 
  update  set jzpr_pay_details.vbdef19 = t.FSTATUSFLAG;
	
--付款单求和后 回写合同的  根据合同pk回写  全量操作
select pay.vbdef20,sum(pay.norigpaymny) fkje from (select * from jzpr_pay_details where vbdef19='1' and dr!=1) pay,JZSUB_CONTRACT ct where pay.vbdef20 = ct.pk_subcontract group by pay.vbdef20

merge into jzsub_contract a 
using (select pay.vbdef20,sum(pay.norigpaymny) fkje from (select * from jzpr_pay_details where vbdef19='1' and dr!=1) pay,JZSUB_CONTRACT ct where pay.vbdef20 = ct.pk_subcontract group by pay.vbdef20) t
on (t.vbdef20 = a.PK_SUBCONTRACT)
when matched then 
  update  set a.noriginsumactlstlmny = t.fkje;
					 

select count(1) from jzsub_contract where dr!=1
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--付款计划表头回写
SELECT pk_payplan,sum(norigpaymny) sum from (select acc.pk_payplan,pay.norigpaymny from jzpr_payplan_acc acc,jzpr_pay_details pay where pay.dr!=1 and pay.vbdef19='1' and pay.vbdef3=acc.pk_payplan_b) GROUP BY pk_payplan

merge into jzpr_payplan
using (SELECT pk_payplan,sum(norigpaymny) sum from (select acc.pk_payplan,pay.norigpaymny from jzpr_payplan_acc acc,jzpr_pay_details pay where pay.dr!=1 and pay.vbdef19='1' and pay.vbdef3=acc.pk_payplan_b) GROUP BY pk_payplan) t
on (jzpr_payplan.PK_PAYPLAN = t.pk_payplan)
when matched then 
  update  set jzpr_payplan.norigpaymny = t.sum;
	
--付款单回写付款计划进度款 本次申请累计付款金额
select sum(norigpaymny) sum,pay.vbdef3 from jzpr_pay_details pay where pay.vbdef3 in (select PK_PAYPLAN_OTH from jzpr_payplan_oth where dr!=1 
and	(norigpaymny is null or norigreqmny>norigpaymny) group by PK_PAYPLAN_OTH) group by pay.vbdef3

merge into jzpr_payplan_oth
using (
select sum(norigpaymny) sum,pay.vbdef3 from jzpr_pay_details pay where pay.dr!=1 and pay.vbdef19='1' and  pay.vbdef3 in (select PK_PAYPLAN_OTH from jzpr_payplan_oth where dr!=1 
and	(norigpaymny is null or norigreqmny>norigpaymny) group by PK_PAYPLAN_OTH) group by pay.vbdef3
) t
on (jzpr_payplan_oth.PK_PAYPLAN_OTH = t.vbdef3)
when matched then 
update  set jzpr_payplan_oth.norigpaymny = t.sum;

--付款单回写付款计划表体结算款 本次申请累计付款金额
merge into jzpr_payplan_settle
using (
select sum(norigpaymny) sum,pay.vbdef3 from jzpr_pay_details pay where pay.dr!=1 and  pay.vbdef19='1' and pay.vbdef3  in (select PK_PAYPLAN_SETTLE from jzpr_payplan_settle where dr!=1 
and	(norigpaymny is null or norigreqmny>norigpaymny) group by PK_PAYPLAN_SETTLE) group by pay.vbdef3) t
on (jzpr_payplan_settle.PK_PAYPLAN_SETTLE = t.vbdef3)
when matched then 
update  set jzpr_payplan_settle.norigpaymny = t.sum;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT vbillcode from jzpr_payplan where pk_payplan in( select pk_payplan from jzpr_payplan_oth where dr!=1 and	(norigpaymny is null or norigreqmny>norigpaymny))

SELECT vbillcode from jzpr_payplan where pk_payplan in( select pk_payplan from jzpr_payplan_settle where dr!=1 and	(norigpaymny is null or norigreqmny>norigpaymny))


select sum(norigpaymny) from jzpr_payplan_oth where dr!=1 and PK_PAYPLAN='1001A21000000000E6C6'


select count(1) from jzpr_payplan_oth where dr!=1 and PK_PAYPLAN='1001A21000000000E6C6'

select acc.pk_payplan,pay.norigpaymny from jzpr_payplan_acc acc,jzpr_pay_details pay where pay.dr!=1 and pay.vbdef19='1' and pay.vbdef3=acc.pk_payplan_b and acc.pk_payplan='1001A21000000000E6C6' 

select acc.pk_payplan,pay.norigpaymny from jzpr_payplan_acc acc,jzpr_pay_details pay where pay.vbdef3=acc.pk_payplan_b and acc.pk_payplan='1001A21000000000E6C6' 
H5502019040100000098

